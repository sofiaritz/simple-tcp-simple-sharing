# TCP Simple Sharing

An easy-to-use software to send the raw bytes of a file using a simple TCP client-server architecture. This is just a 
proof of concept for a future blogpost where I'll be exploring multiple options to share files between two peers.

This is what's going to be called the _Simple TCP approach_. It's just a simple [egui](https://docs.rs/eframe/latest/eframe/)
application that interfaces a simple [TCP server](src/networking/server/mod.rs) (receiver) and a simple 
[TCP Client](src/networking/client/mod.rs) (sender).

## Features

- (Generally) faster than HTTP-server method
  - The [_Simple approach_](https://codeberg.org/sofiaritz/http-simple-sharing) was made thinking of peers just sharing
  the link to other peers, and because the server isn't HTTPS, modern browser sometimes have a hard time downloading
  the files.

## Drawbacks

- Requires both peers to download this software (which is expected for anything outside HTTP btw)
- No encryption
- Has a harder time with [file formats](https://codeberg.org/sofiaritz/tcp-simple-sharing/src/commit/5d8b44ab975808fff598c557defd211a02a371a6/src/main.rs#L315)

## Expectations

This project was done in just a day. Code quality isn't the best. I may improve this proof of concept (prevent copying
and cloning of buffers, which have a huge hit in performance; improve multithreading handling, etc), but I won't be
adding any features because that's going to be on the _Advanced TCP approach_.

However, with that being said, if you'd like to contribute or fix something, go for it! :)