use std::sync::{Arc, RwLock};
use std::{fs, thread};
use std::net::{IpAddr};
use std::ops::Deref;
use std::path::PathBuf;
use std::str::FromStr;
use eframe::{egui, Frame};
use eframe::egui::{Context, RichText};
use crate::networking::client::start_client;
use crate::networking::server::start_server;

mod networking;

#[tokio::main]
async fn main() {
    let options = eframe::NativeOptions {
        initial_window_size: Some(egui::vec2(640.0, 640.0)),
        ..Default::default()
    };

    let ip = Arc::new(public_ip::addr().await.expect("failed to get ip addr"));
    eframe::run_native(
        "Direct sharing",
        options,
        Box::new(|_| Box::new(App {
            public_ip: ip,
            receiver_port: 8069,
            lease_hours: 3,
            picked_file: None,
            show_ip: false,
            receiver_port_buffer: "8069".into(),
            receiver_status: Arc::new(RwLock::new(ReceiverStatus::Stopped)),
            receiver_file_saved: false,

            sender_ip_buffer: "1.1.1.1".into(),
            sender_ip: Arc::new(IpAddr::from([1, 1, 1, 1])),
            sender_port: 8069,
            sender_port_buffer: "8069".into(),
            sender_status: Arc::new(RwLock::new(SenderStatus::Stopped)),
        }))
    )
}

#[derive(Clone)]
pub struct File {
    pub path: PathBuf,
    pub name: String,
}

enum ReceiverStatus {
    Stopped,
    Started,
    Errored(anyhow::Error),
    Data(Vec<u8>),
}

enum SenderStatus {
    Stopped,
    Started,
    ValidationError(String),
    Errored(String),
}

struct App {
    public_ip: Arc<IpAddr>,
    receiver_port: u16,
    lease_hours: u32,
    picked_file: Option<File>,
    show_ip: bool,
    receiver_port_buffer: String,
    receiver_file_saved: bool,
    receiver_status: Arc<RwLock<ReceiverStatus>>,

    sender_ip_buffer: String,
    sender_ip: Arc<IpAddr>,
    sender_port_buffer: String,
    sender_port: u16,
    sender_status: Arc<RwLock<SenderStatus>>,
}

impl eframe::App for App {
    fn update(&mut self, ctx: &Context, _frame: &mut Frame) {
        egui::CentralPanel::default().show(ctx, |ui| {
            ui.label(RichText::new("Direct sharing").size(20.0));

            ui.group(|ui| {
                ui.set_width(610.0);

                ui.label(RichText::new("Sender").size(16.0));
                ui.add_space(5.0);

                ui.label(RichText::new("File to send").size(14.0));
                ui.add_space(2.0);

                if let Some(file) = &self.picked_file {
                    ui.horizontal(|ui| {
                        ui.label("File name:");
                        ui.monospace(&file.name);
                        ui.separator();
                        ui.label("Path:");
                        ui.monospace(&file.path.display().to_string());
                    });
                } else {
                    ui.label(RichText::new("No file selected").italics());
                }

                if ui.button("Open file...").clicked() {
                    if let Some(path) = rfd::FileDialog::new().pick_file() {
                        let file_name = path.file_name().unwrap().to_str().unwrap().to_string();
                        self.picked_file = Some(File {
                            name: file_name,
                            path,
                        });
                    }
                }

                ui.separator();

                ui.label(RichText::new("Connection settings").size(14.0));
                ui.add_space(2.0);

                ui.horizontal(|ui| {
                    ui.label("IP:");
                    ui.text_edit_singleline(&mut self.sender_ip_buffer);

                    ui.monospace(&self.sender_ip_buffer);

                    let value = {
                        let v = self.sender_status.read().unwrap();
                        // TODO: This is a workaround to get an _actual_ clone of the *value*, find
                        // an efficient way to do this!
                        match v.deref() {
                            SenderStatus::Stopped => SenderStatus::Stopped,
                            SenderStatus::ValidationError(v) => SenderStatus::ValidationError(v.clone()),
                            SenderStatus::Started => SenderStatus::Started,
                            SenderStatus::Errored(v) => SenderStatus::Errored(v.clone()),
                        }
                    };

                    if let SenderStatus::ValidationError(err) = value {
                        if err.starts_with("IP: ") {
                            // TODO: Use error message
                            self.sender_ip_buffer = self.sender_ip.to_string();
                            *self.sender_status.write().unwrap() = SenderStatus::Stopped;
                        }
                    }
                });

                ui.horizontal(|ui| {
                    ui.label("Port:");
                    match self.sender_status.read().unwrap().deref() {
                        SenderStatus::Started => {}
                        _ => {
                            ui.text_edit_singleline(&mut self.sender_port_buffer);

                            self.sender_port_buffer = self.sender_port_buffer.chars().filter(|c| c.is_ascii_digit()).collect();
                            if self.sender_port_buffer.is_empty() {
                                self.sender_port_buffer = u16::MAX.to_string();
                            }

                            let input = self.sender_port_buffer.parse::<usize>().expect("invalid port");
                            if self.sender_port as usize != input {
                                if input > u16::MAX as usize {
                                    self.sender_port_buffer = u16::MAX.to_string();
                                } else {
                                    self.sender_port = self.sender_port_buffer.parse::<u16>().expect("invalid port");
                                }
                            }
                        }
                    }

                    ui.monospace(self.sender_port.to_string());
                });

                if let Some(file) = &self.picked_file {
                    if ui.button("Send file").clicked() {
                        match IpAddr::from_str(&self.sender_ip_buffer) {
                            Ok(v) => self.sender_ip = Arc::new(v),
                            Err(_) => {
                                self.sender_ip = Arc::new(IpAddr::from([1, 1, 1, 1]));
                                *self.sender_status.write().unwrap() = SenderStatus::ValidationError("IP: Invalid IP".into());
                            },
                        }

                        let sender_status = self.sender_status.clone();
                        let ip = self.sender_ip.clone();
                        let port = self.sender_port;
                        let data = fs::read(file.path.clone()).expect("failed to read file");

                        thread::spawn(move || {
                            {
                                *sender_status.write().unwrap() = SenderStatus::Started;
                            }

                            match start_client(&ip, port, &data) {
                                Ok(_) => *sender_status.write().unwrap() = SenderStatus::Stopped,
                                Err(error) => *sender_status.write().unwrap() = SenderStatus::Errored(error.to_string()),
                            }
                        });
                    }
                } else {
                    ui.label(RichText::new("Pick a file").italics());
                }

                if let SenderStatus::Errored(v) = &self.sender_status.read().unwrap().deref() {
                    ui.label(v);
                }
            });

            ui.group(|ui| {
                ui.set_width(610.0);

                ui.label(RichText::new("Receiver").size(16.0));
                ui.add_space(5.0);

                ui.horizontal(|ui| {
                    ui.label("Public IP:");
                    if self.show_ip {
                        ui.monospace(self.public_ip.to_string());
                        if ui.button("Hide IP").clicked() {
                            self.show_ip = false;
                        }
                    } else if ui.button("Show IP").clicked() {
                        self.show_ip = true;
                    }
                });

                ui.horizontal(|ui| {
                    ui.label("Port:");
                    match self.receiver_status.read().unwrap().deref() {
                        ReceiverStatus::Started => {}
                        _ => {
                            ui.text_edit_singleline(&mut self.receiver_port_buffer);

                            self.receiver_port_buffer = self.receiver_port_buffer.chars().filter(|c| c.is_ascii_digit()).collect();
                            if self.receiver_port_buffer.is_empty() {
                                self.receiver_port_buffer = u16::MAX.to_string();
                            }

                            let input = self.receiver_port_buffer.parse::<usize>().expect("invalid port");
                            if self.receiver_port as usize != input {
                                if input > u16::MAX as usize {
                                    self.receiver_port_buffer = u16::MAX.to_string();
                                } else {
                                    self.receiver_port = self.receiver_port_buffer.parse::<u16>().expect("invalid port");
                                }
                            }
                        }
                    }

                    ui.monospace(self.receiver_port.to_string());
                });

                ui.horizontal(|ui| {
                    ui.label("Lease hours:");
                    ui.monospace(self.lease_hours.to_string());
                });

                match &self.receiver_status.read().unwrap().deref() {
                    ReceiverStatus::Stopped => {
                        if ui.button("Start receiver").clicked() {
                            let server_status = self.receiver_status.clone();
                            let port = self.receiver_port;
                            let lease_hours = self.lease_hours;

                            self.receiver_file_saved = false;
                            thread::spawn(move || {
                                {
                                    *server_status.write().unwrap() = ReceiverStatus::Started;
                                }

                                match start_server(port, lease_hours) {
                                    Ok(data) => *server_status.write().unwrap() = ReceiverStatus::Data(data),
                                    Err(error) => *server_status.write().unwrap() = ReceiverStatus::Errored(error),
                                }
                            });
                        }
                    }
                    ReceiverStatus::Started => {
                        ui.label(RichText::new("Receiver started, close the window to stop the server").italics());
                    }
                    ReceiverStatus::Errored(error) => {
                        ui.label(RichText::new("Server errored!").strong().italics());
                        ui.monospace(error.to_string());
                    }
                    ReceiverStatus::Data(data) => {
                        ui.horizontal(|ui| {
                            ui.label("Data received! Length:");
                            ui.monospace(data.len().to_string());
                        });

                        if !self.receiver_file_saved {
                            save_file(data);
                            self.receiver_file_saved = true;
                        }
                    }
                }
            });
        });
    }
}

fn save_file(data: &[u8]) {
    if let Some(file) = rfd::FileDialog::new()
        .add_filter("File", &calculate_filter(data))
        .set_file_name("received")
        .save_file()
    {
        fs::write(file, data).unwrap();
    } else {
        save_file(data)
    }
}

fn calculate_filter(data: &[u8]) -> Vec<&str> {
    if data.starts_with(&[0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A]) { // PNG
        vec!["png"]
    } else if data[6..=9].starts_with(&[0xFF, 0xD8, 0xFF]) { // JPEG common
        vec!["jpg"]
    } else if data.starts_with(&[0x50, 0x4B]) { // ZIP
        vec!["zip", "docx", "epub", "jar", "odp", "ods", "odt", "pptx"]
    } else if data.starts_with(&[0x52, 0x61, 0x72, 0x21, 0x1A, 0x07]) { // RAR common
        vec!["rar"]
    } else if data.starts_with(&[0x37, 0x7A, 0xBC, 0xAF, 0x27, 0x1C]) { // 7Z
        vec!["7z"]
    } else if data.starts_with(&[0xCA, 0xFE, 0xBA, 0xBE]) { // .class
        vec!["class"]
    } else if data.starts_with(&[0xEF, 0xBB, 0xBF]) ||
        data.starts_with(&[0xFF, 0xFE]) ||
        data.starts_with(&[0xFE, 0xFF]) ||
        data.starts_with(&[0xFF, 0xFE, 0x00, 0x00]) ||
        data.starts_with(&[0x00, 0x00, 0xFE, 0xFF])
    { // UTF-8, UTF16(BE,LE) and UTF32(BE,LE) BOM
        vec!["txt"]
    } else if data.starts_with(&[0x25, 0x50, 0x44, 0x46, 0x2D]) { // PDF
        vec!["pdf"]
    } else if data.starts_with(&[0xFF, 0xFB]) ||
        data.starts_with(&[0xFF, 0xF3]) ||
        data.starts_with(&[0xFF, 0xF2]) ||
        data.starts_with(&[0x49, 0x44, 0x33])
    { // MP3
        vec!["mp3"]
    } else if data.starts_with(&[0x66, 0x4C, 0x61, 0x43]) { // FLAC
        vec!["flac"]
    } else if data.starts_with(&[0x4D, 0x54, 0x68, 0x64]) { // MIDI
        vec!["midi", "mid"]
    } else if data.starts_with(&[0xD0, 0xCF, 0x11, 0xE0, 0xA1, 0xB1, 0x1A, 0xE1]) { // CFBF (MSI and old Office)
        vec!["msi", "doc", "xls", "ppt", "msg"]
    } else if data.starts_with(&[0x43, 0x72, 0x32, 0x34]) { // CRX
        vec!["crx"]
    } else if data.starts_with(&[0x4E, 0x45, 0x53, 0x1A]) { // NES
        vec!["nes"]
    } else if data.starts_with(&[0x1F, 0x8B]) { // GZ
        vec!["gz", "tar.gz"]
    } else if data.starts_with(&[0xFD, 0x37, 0x7A, 0x58, 0x5A, 0x00]) { // XZ
        vec!["xz", "tar.xz"]
    } else if data.starts_with(&[0x1A, 0x45, 0xDF, 0xA3]) { // Matroska media container
        vec!["mkv", "webm"]
    } else if data.starts_with(&[0x66, 0x74, 0x79, 0x70, 0x69, 0x73, 0x6F, 0x6D]) { // MP4
        vec!["mp4"]
    } else if data.starts_with(&[0x52, 0x49, 0x46, 0x46]) { // Start of WAV, AVI, WebP formats
        if data[8..=11].starts_with(&[0x57, 0x41, 0x56, 0x45]) { // WAV
            vec!["wav"]
        } else if data[8..=11].starts_with(&[0x41, 0x56, 0x49, 0x20]) { // AVI
            vec!["avi"]
        } else if data[8..=11].starts_with(&[0x57, 0x45, 0x42, 0x50]) { // WebP
            vec!["webp"]
        } else {
            vec![]
        }
    } else {
        vec![]
    }
}