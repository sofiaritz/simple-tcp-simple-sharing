use std::{io};
use std::io::BufReader;
use std::io::prelude::*;
use local_ip_address::local_ip as get_local_ip;
use std::net::{IpAddr, SocketAddrV4, TcpListener, TcpStream};

pub fn start_server(port: u16, lease_hours: u32) -> anyhow::Result<Vec<u8>> {
    println!("server");
    let local_port = port - 10;
    let local_ip = match get_local_ip().unwrap() {
        IpAddr::V4(v) => v,
        IpAddr::V6(_) => anyhow::bail!("failed to get local ip"),
    };

    let gateway = igd::search_gateway(Default::default())?;
    gateway.add_port(
        igd::PortMappingProtocol::TCP,
        port,
        SocketAddrV4::new(local_ip, local_port),
        lease_hours * 60 * 60,
        "Simple Sharing Server"
    )?;

    println!("server igd");
    let listener = TcpListener::bind(format!("{local_ip}:{local_port}"))?;

    if let Some(stream) = listener.incoming().next() {
        let stream = stream?;

        return Ok(handle_connection(&stream)?)
    }

    println!("server end");
    Ok(vec![])
}

fn handle_connection(mut stream: &TcpStream) -> io::Result<Vec<u8>> {
    let mut buf_reader = BufReader::new(&mut stream);
    let mut file = vec![];
    buf_reader.read_to_end(&mut file)?;

    Ok(file)
}
