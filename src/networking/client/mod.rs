use std::{io};
use std::io::{Write};
use std::net::{IpAddr, TcpStream};

pub fn start_client(ip_addr: &IpAddr, port: u16, file: &[u8]) -> io::Result<()> {
    println!("client");
    let mut stream = TcpStream::connect(format!("{ip_addr}:{port}"))?;

    stream.write_all(file)?;
    stream.flush()?;

    println!("client end");
    Ok(())
}